package br.com.easolutions.cyclingcoach.comons;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;


/**
 * Created by Elvis Aron Andrade Development Mobile on 11/08/2018 at 12:28.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class GenericDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return builder.create();
    }


}
