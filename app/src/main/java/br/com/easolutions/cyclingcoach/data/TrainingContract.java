package br.com.easolutions.cyclingcoach.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Elvis Aron Andrade Development Mobile on 11/08/2018 at 13:49.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class TrainingContract {

    public static final String AUTHORITY = "br.com.easolutions.cyclingcoach";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    public static final String PATH_TRAININGS = "trainings";

    public static final class TrainingEntry implements BaseColumns {
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_TRAININGS).build();

        public static final String TABLE_NAME = "trainings";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_MINUTES = "minutes";
        public static final String COLUMN_BPM = "bpm";
    }

}
