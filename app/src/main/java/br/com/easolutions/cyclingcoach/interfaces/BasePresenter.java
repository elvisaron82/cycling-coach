package br.com.easolutions.cyclingcoach.interfaces;

public interface BasePresenter {
    void start();
}
