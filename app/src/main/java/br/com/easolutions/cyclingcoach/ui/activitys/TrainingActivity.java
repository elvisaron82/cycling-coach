package br.com.easolutions.cyclingcoach.ui.activitys;

import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.R;
import br.com.easolutions.cyclingcoach.adapters.TrainingListFormAdapter;
import br.com.easolutions.cyclingcoach.model.TrainingData;
import br.com.easolutions.cyclingcoach.presenters.TrainingPresenter;
import br.com.easolutions.cyclingcoach.ui.fragments.TrainingFragment;
import br.com.easolutions.cyclingcoach.utils.ActivityUtils;

import static br.com.easolutions.cyclingcoach.presenters.TrainingPresenter.PARCELABLE_START_TRAINING_KEY;

public class TrainingActivity extends AppCompatActivity implements TrainingFragment.OnFragmentTrainingListener{

    public static final int ONE_MINUTE = 60;

    private TrainingFragment mTrainingFragment;
    private TrainingPresenter mTrainingPresenter;


    @Override
    protected final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);


        ArrayList<TrainingData> trainingData = getParcelableTrainingArrayList(getIntent());
        showTrainingFragment();

        mTrainingPresenter = new TrainingPresenter(mTrainingFragment);
        mTrainingPresenter.setIntentParcelableTrainingArrayList(trainingData);


        final Button btStart = findViewById(R.id.bt_start);
        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTrainingPresenter.verifyIfICallStopwatch();
                btStart.setVisibility(View.GONE);
            }
        });
    }

    private void showTrainingFragment() {
        mTrainingFragment =
                (TrainingFragment) getSupportFragmentManager().findFragmentById(R.id.fl_training);

        if (mTrainingFragment == null) {
            mTrainingFragment = TrainingFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), mTrainingFragment, R.id.fl_training);
        }
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public ArrayList<TrainingData> getParcelableTrainingArrayList(Intent intent) {
        ArrayList<TrainingData> trainingData = new ArrayList<>();
        if (intent.hasExtra(PARCELABLE_START_TRAINING_KEY)) {
            if (intent.getExtras() != null) {
                trainingData = intent.getExtras().getParcelableArrayList(PARCELABLE_START_TRAINING_KEY);
            }
        }
        return trainingData;
    }
}
