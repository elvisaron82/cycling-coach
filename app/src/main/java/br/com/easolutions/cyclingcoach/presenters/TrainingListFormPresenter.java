package br.com.easolutions.cyclingcoach.presenters;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.interfaces.TrainingListFormContract;
import br.com.easolutions.cyclingcoach.model.TrainingData;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Elvis Aron Andrade Development Mobile on 11/08/2018 at 11:52.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class TrainingListFormPresenter implements TrainingListFormContract.Presenter{

    private ArrayList<TrainingData> mTrainingDataList = new ArrayList<>();

    private TrainingListFormContract.View mView;

    public TrainingListFormPresenter(@NonNull TrainingListFormContract.View view) {
        mView = checkNotNull(view, "MainActivity view cannot be null!");
    }

    @Override
    public void inputDataTrainingForm(String minutes, String bpm, String cadence) {
        boolean error = validateTrainingForm(minutes, bpm);

        if (error) {
            mView.showDialogError();
        } else {

            int min = Integer.parseInt(minutes);
            int bat = Integer.parseInt(bpm);
            int cad = Integer.parseInt(cadence);

            TrainingData trainingData = new TrainingData();
            trainingData.setMinutes(min);
            trainingData.setBmp(bat);
            trainingData.setCadence(cad);
            trainingData.setItemFinalized(false);

            mTrainingDataList.add(trainingData);
            mView.insertTrainingData(mTrainingDataList);
            mView.cleanEdittext();
        }
    }

    @Override
    public void saveWorkout() {
        mView.openTrainingStartActivity(mTrainingDataList);
    }

    private boolean validateTrainingForm(String minutes, String bpm) {
        boolean error = false;
        if (minutes.isEmpty()) {
            error = true;
        } else {
            if (Integer.parseInt(minutes) <= 0) {
                error = true;
            }
        }

        if (bpm.isEmpty()) {
            error = true;
        } else {
            if (Integer.parseInt(bpm) <= 0) {
                error = true;
            }
        }
        return error;
    }

    @Override
    public ArrayList<TrainingData> getTrainingDataList() {
        return mTrainingDataList;
    }
}
