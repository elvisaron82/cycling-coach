package br.com.easolutions.cyclingcoach.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.R;
import br.com.easolutions.cyclingcoach.adapters.TrainingListFormAdapter;
import br.com.easolutions.cyclingcoach.interfaces.TrainingListFormContract;
import br.com.easolutions.cyclingcoach.model.TrainingData;
import br.com.easolutions.cyclingcoach.presenters.TrainingListFormPresenter;

public class TrainingListFormFragment extends Fragment implements TrainingListFormContract.View {

    private OnAddFormDataListener mListener;
    private TrainingListFormAdapter mTrainingListFormAdapter;
    private TrainingListFormPresenter mPresenter;
    private EditText mEtMinute;
    private EditText mEtBpm;
    private EditText mCadenceEditText;

    public TrainingListFormFragment() { }

    public static TrainingListFormFragment newInstance() {
        TrainingListFormFragment fragment = new TrainingListFormFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTrainingListFormAdapter = new TrainingListFormAdapter();
        mPresenter = new TrainingListFormPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout =  inflater.inflate(R.layout.fragment_list_training_form, container, false);

        mEtMinute = layout.findViewById(R.id.et_minute);
        mEtBpm = layout.findViewById(R.id.et_bpm);
        mCadenceEditText = layout.findViewById(R.id.et_cadence);
        ImageButton ibAdd = layout.findViewById(R.id.ib_add);

        ibAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onButtonPressed(mEtMinute.getText().toString(),
                        mEtBpm.getText().toString(),
                        mCadenceEditText.getText().toString());
            }
        });

        final Button btSaveWorkout = layout.findViewById(R.id.bt_save_workout);

        btSaveWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWorkoutSaved();
            }
        });

        RecyclerView recyclerView = layout.findViewById(R.id.rv_list_training_form);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mTrainingListFormAdapter);

        return layout;
    }


    private void openWorkoutSaved() {
        mPresenter.saveWorkout();
    }


    private void onButtonPressed(String minutes, String bpm, String cadence) {
        mPresenter.inputDataTrainingForm(minutes, bpm, cadence);
    }

    @Override
    public void showDialogError() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_error)
                .setTitle(R.string.error);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void insertTrainingData(ArrayList<TrainingData> trainingArrayList) {
        mTrainingListFormAdapter.setTrainingList(trainingArrayList);
    }

    @Override
    public void cleanEdittext() {
        mEtMinute.getText().clear();
        mEtBpm.getText().clear();
    }

    @Override
    public void openTrainingStartActivity(ArrayList<TrainingData> trainingDataList) {
        mListener.onAddFormData(trainingDataList);
    }

    public interface OnAddFormDataListener {
        void onAddFormData(ArrayList<TrainingData> trainingDataArrayList);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TrainingListFormFragment.OnAddFormDataListener) {
            mListener = (TrainingListFormFragment.OnAddFormDataListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddFormDataListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
