package br.com.easolutions.cyclingcoach.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Elvis Aron Andrade Development Mobile on 10/08/2018 at 22:26.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class TrainingData implements Parcelable {

    @SerializedName("minutes")
    @Expose
    private Integer minutes;
    @SerializedName("bpm")
    @Expose
    private Integer bmp;
    @SerializedName("itemFinalized")
    @Expose
    private boolean itemFinalized;
    @SerializedName("cadence")
    @Expose
    private Integer cadence;

    public TrainingData() {}

    protected TrainingData(Parcel in) {
        minutes = ((Integer) in.readValue((Integer.class.getClassLoader())));
        bmp = ((Integer) in.readValue((Integer.class.getClassLoader())));
        itemFinalized = in.readInt() != 0;
        cadence = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public static final Creator<TrainingData> CREATOR = new Creator<TrainingData>() {
        @Override
        public TrainingData createFromParcel(Parcel in) {
            return new TrainingData(in);
        }

        @Override
        public TrainingData[] newArray(int size) {
            return new TrainingData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(minutes);
        parcel.writeValue(bmp);
        parcel.writeInt(itemFinalized ? 1 : 0);
        parcel.writeValue(cadence);
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setBmp(Integer bmp) {
        this.bmp = bmp;
    }

    public Integer getBmp() {
        return bmp;
    }

    public void setItemFinalized(boolean itemFinalized) {
        this.itemFinalized = itemFinalized;
    }

    public boolean getItemFinalized() {
        return itemFinalized;
    }

    public void setCadence(Integer cadence) {
        this.cadence = cadence;
    }

    public Integer getCadence() {
        return cadence;
    }
}
