package br.com.easolutions.cyclingcoach.interfaces;

public interface BaseView <T extends BasePresenter> {
    void setPresenter(T presenter);
}