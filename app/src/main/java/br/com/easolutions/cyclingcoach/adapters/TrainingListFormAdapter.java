package br.com.easolutions.cyclingcoach.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.R;
import br.com.easolutions.cyclingcoach.model.TrainingData;

/**
 * Created by Elvis Aron Andrade Development Mobile on 11/08/2018 at 18:53.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class TrainingListFormAdapter extends RecyclerView.Adapter<TrainingListFormAdapter.TrainingFormListViewHolder>
    implements HeaderItemDecoration.StickyHeaderInterface {

    private ArrayList<TrainingData> mTrainingDataList = new ArrayList<>();


    @NonNull
    @Override
    public TrainingFormListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.item_training_form;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new TrainingListFormAdapter.TrainingFormListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainingFormListViewHolder holder, int position) {
        holder.bindData(mTrainingDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return mTrainingDataList.size();
    }

    public void setTrainingList(ArrayList<TrainingData> trainingDataList){
        mTrainingDataList = trainingDataList;
        notifyDataSetChanged();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 0;
        do {
            if (this.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        } while (itemPosition >= 0);
        return headerPosition;
    }

    @Override
    public int getHeaderLayout(int headerPosition) {
        return 0;
    }

    @Override
    public void bindHeaderData(View header, int headerPosition) {

    }

    @Override
    public boolean isHeader(int itemPosition) {
        return false;
    }

    public class TrainingFormListViewHolder extends RecyclerView.ViewHolder{
        private final TextView mCadenceTextView;
        private final TextView mTextViewMinutes;
        private final TextView mTextViewBpm;
        private final ImageView mCheckImageView;


        public TrainingFormListViewHolder(View itemView) {
            super(itemView);
            mCadenceTextView = itemView.findViewById(R.id.tv_cadence);
            mTextViewMinutes = itemView.findViewById(R.id.tv_minute);
            mTextViewBpm = itemView.findViewById(R.id.tv_bpm);
            mCheckImageView = itemView.findViewById(R.id.iv_check);
        }

        public void bindData(TrainingData trainingData) {
            mCadenceTextView.setText(trainingData.getCadence());
            mTextViewMinutes.setText(String.valueOf(trainingData.getMinutes()));
            mTextViewBpm.setText(String.valueOf(trainingData.getBmp()));
            if (trainingData.getItemFinalized()) {
                mCheckImageView.setVisibility(View.VISIBLE);
            }
        }
    }

    public synchronized void updateItem(int position, TrainingData trainingData){
        if (position >= 0) {
            mTrainingDataList.set(position, trainingData);
            notifyItemChanged(position);
        }
    }
}
