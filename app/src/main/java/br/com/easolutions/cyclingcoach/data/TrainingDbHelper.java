package br.com.easolutions.cyclingcoach.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static br.com.easolutions.cyclingcoach.data.TrainingContract.TrainingEntry.COLUMN_BPM;
import static br.com.easolutions.cyclingcoach.data.TrainingContract.TrainingEntry.COLUMN_MINUTES;
import static br.com.easolutions.cyclingcoach.data.TrainingContract.TrainingEntry.TABLE_NAME;
import static br.com.easolutions.cyclingcoach.data.TrainingContract.TrainingEntry._ID;

/**
 * Created by Elvis Aron Andrade Development Mobile on 11/08/2018 at 13:57.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class TrainingDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "trainingDb.db";
    private static final int VERSION = 1;

    TrainingDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String CREATE_TABLE = "CREATE TABLE "  + TABLE_NAME + " (" +
                _ID                + " INTEGER PRIMARY KEY, " +
                COLUMN_MINUTES + " INTEGER NOT NULL, " +
                COLUMN_BPM    + " INTEGER NOT NULL);";

        sqLiteDatabase.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
