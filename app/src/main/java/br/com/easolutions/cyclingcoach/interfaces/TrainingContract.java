package br.com.easolutions.cyclingcoach.interfaces;

import android.content.Intent;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.model.TrainingData;

public interface TrainingContract {
    interface View extends BaseView<Presenter>{
        void setTextTimer(String minutesFormated);
        void scrollToPosition(int position);
        void updateItem(int position, TrainingData trainingData);
        void setIdealHeartHate(String heartHate);
    }

    interface Presenter extends BasePresenter{
        void verifyIfICallStopwatch();
        ArrayList<TrainingData> getTrainingDataArrayList();
    }
}
