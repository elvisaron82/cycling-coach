package br.com.easolutions.cyclingcoach.interfaces;

import android.content.ContentValues;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.model.TrainingData;

/**
 * Created by Elvis Aron Andrade Development Mobile on 11/08/2018 at 11:47.
 * EA Solutions
 * evs.aron@gmail.com
 */
public interface TrainingListFormContract {

    interface View {
        void showDialogError();
        void cleanEdittext();
        void openTrainingStartActivity(ArrayList<TrainingData> mTrainingDataList);
        void insertTrainingData(ArrayList<TrainingData> trainingArrayList);
    }

    interface Presenter {
        void inputDataTrainingForm(String minutes, String bpm, String cadence);
        void saveWorkout();

        ArrayList<TrainingData> getTrainingDataList();
    }
}
