package br.com.easolutions.cyclingcoach.presenters;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.interfaces.TrainingContract;
import br.com.easolutions.cyclingcoach.model.TrainingData;
import br.com.easolutions.cyclingcoach.ui.activitys.TrainingActivity;

import static com.google.common.base.Preconditions.checkNotNull;

public class TrainingPresenter implements TrainingContract.Presenter {

    public static final String PARCELABLE_START_TRAINING_KEY = "parcelable_start_training_key";
    private ArrayList<TrainingData> mTrainingDataArrayList;
    private int mItemCurrentListTraining;
    private int mCurrentTimeCounter;
    private TrainingContract.View mTrainingView;

    public TrainingPresenter(@NonNull TrainingContract.View trainingView) {
        mTrainingView = checkNotNull(trainingView, "trainingView cannot be null!");
        mTrainingView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    public void setIntentParcelableTrainingArrayList(ArrayList<TrainingData> trainingArrayList) {
        mTrainingDataArrayList = trainingArrayList;
    }

    @Override
    public void verifyIfICallStopwatch() {
        if (mTrainingDataArrayList.size() > mItemCurrentListTraining) {
            final TrainingData trainingMinutes = mTrainingDataArrayList.get(mItemCurrentListTraining);
            final Integer minutes = trainingMinutes.getMinutes();
            mCurrentTimeCounter = minutes * TrainingActivity.ONE_MINUTE;
            final int timeLeft = this.mCurrentTimeCounter * 1000;
            startTraining(timeLeft);

            setCurrentWorkoutItem();
            mTrainingView.scrollToPosition(mItemCurrentListTraining);

            String heartHate = String.valueOf(mTrainingDataArrayList.get(mItemCurrentListTraining).getBmp());
            mTrainingView.setIdealHeartHate(heartHate);
        }
    }

    private void setCurrentWorkoutItem() {

    }

    @Override
    public ArrayList<TrainingData> getTrainingDataArrayList() {
        return mTrainingDataArrayList;
    }

    private class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(int timeLeft) {
            super(timeLeft, 1000);
        }

        public void onTick(long millisUntilFinished){

            String minutesFormated = DateUtils.formatElapsedTime(mCurrentTimeCounter);
            mTrainingView.setTextTimer(minutesFormated);
            mCurrentTimeCounter--;
        }

        public  void onFinish(){
            mTrainingView.setTextTimer("FINISH!!");
            mTrainingDataArrayList.get(mItemCurrentListTraining).setItemFinalized(true);
            mTrainingView.updateItem(mItemCurrentListTraining, mTrainingDataArrayList.get(mItemCurrentListTraining));
            mItemCurrentListTraining++;
            verifyIfICallStopwatch();
        }
    }

    private void startTraining(final int timeLeft) {
        MyCountDownTimer myCountDownTimer = new MyCountDownTimer(timeLeft);
        myCountDownTimer.start();
    }


}
