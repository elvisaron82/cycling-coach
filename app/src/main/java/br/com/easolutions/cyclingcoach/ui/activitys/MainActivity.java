package br.com.easolutions.cyclingcoach.ui.activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.R;
import br.com.easolutions.cyclingcoach.model.TrainingData;
import br.com.easolutions.cyclingcoach.ui.fragments.TrainingListFormFragment;
import br.com.easolutions.cyclingcoach.ui.fragments.TrainingListFormFragment.OnAddFormDataListener;
import br.com.easolutions.cyclingcoach.utils.ActivityUtils;

import static br.com.easolutions.cyclingcoach.presenters.TrainingPresenter.PARCELABLE_START_TRAINING_KEY;

public class MainActivity extends AppCompatActivity
        implements OnAddFormDataListener {

    private TrainingListFormFragment mTrainingListFormFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.addFormaTrainingListFragment();
    }

    private void addFormaTrainingListFragment() {
        this.mTrainingListFormFragment =
                (TrainingListFormFragment) this.getSupportFragmentManager().findFragmentById(R.id.fl_list_training);

        if (this.mTrainingListFormFragment == null) {
            this.mTrainingListFormFragment = TrainingListFormFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    this.getSupportFragmentManager(), this.mTrainingListFormFragment, R.id.fl_list_training);
        }
    }

    @Override
    public void onAddFormData(ArrayList<TrainingData> trainingDataArrayList) {
        //TrainingActivity
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(PARCELABLE_START_TRAINING_KEY, trainingDataArrayList);
        Intent startTraining = new Intent(this, TrainingActivity.class);
        startTraining.putExtras(bundle);
        this.startActivity(startTraining);
    }

}
