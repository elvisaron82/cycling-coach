package br.com.easolutions.cyclingcoach.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by Elvis Aron Andrade Development Mobile on 12/08/2018 at 12:09.
 * EA Solutions
 * evs.aron@gmail.com
 */
public class CyclingCoachDateUtils {

    public static final long DAY_IN_MILLIS = TimeUnit.DAYS.toMillis(1);

    public static boolean isDateNormalized(long millisSinceEpoch) {
        boolean isDateNormalized = false;
        if (millisSinceEpoch % DAY_IN_MILLIS == 0) {
            isDateNormalized = true;
        }

        return isDateNormalized;
    }
}
