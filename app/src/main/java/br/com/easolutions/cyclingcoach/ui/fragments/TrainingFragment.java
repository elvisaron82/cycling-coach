package br.com.easolutions.cyclingcoach.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.easolutions.cyclingcoach.R;
import br.com.easolutions.cyclingcoach.adapters.TrainingListFormAdapter;
import br.com.easolutions.cyclingcoach.interfaces.TrainingContract;
import br.com.easolutions.cyclingcoach.model.TrainingData;
import br.com.easolutions.cyclingcoach.presenters.TrainingListFormPresenter;

import static br.com.easolutions.cyclingcoach.presenters.TrainingPresenter.PARCELABLE_START_TRAINING_KEY;
import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

public class TrainingFragment extends Fragment implements TrainingContract.View{

    private static final String KEY_ARG_TRAINING = "key_arg_training";
    private OnFragmentTrainingListener mListener;
    private TrainingContract.Presenter mPresenter;
    private TextView mTimerTextView;
    private TextView mHeartHateTextView;
    private RecyclerView mListTrainingRecyclerView;
    private TrainingListFormAdapter mTrainingListFormAdapter;

    public TrainingFragment() {}

    public static TrainingFragment newInstance() {
        return new TrainingFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTrainingListFormAdapter = new TrainingListFormAdapter();
        mTrainingListFormAdapter.setTrainingList(mPresenter.getTrainingDataArrayList());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_training, container, false);
        mTimerTextView = (TextView) layout.findViewById(R.id.tv_time_left);
        mHeartHateTextView = (TextView) layout.findViewById(R.id.tv_heart_rate);

        mListTrainingRecyclerView = (RecyclerView) layout.findViewById(R.id.rv_list_training);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        mListTrainingRecyclerView.setLayoutManager(layoutManager);
        mListTrainingRecyclerView.setHasFixedSize(true);
        mListTrainingRecyclerView.setAdapter(mTrainingListFormAdapter);
        return layout;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentTrainingListener) {
            mListener = (OnFragmentTrainingListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentTrainingListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setPresenter(TrainingContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void setTextTimer(String minutesFormated) {
        mTimerTextView.setText(minutesFormated);
    }

    public interface OnFragmentTrainingListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void scrollToPosition(int position) {
        mListTrainingRecyclerView.scrollToPosition(position);
    }

    @Override
    public void updateItem(int position, TrainingData trainingData) {
        mTrainingListFormAdapter.updateItem(position, trainingData);
    }

    @Override
    public void setIdealHeartHate(String heartHate) {
        mHeartHateTextView.setText(heartHate);
    }
}
